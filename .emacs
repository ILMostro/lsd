(put 'narrow-to-page 'disabled nil)

(require 'package)
;;(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)                ;; Initialize & Install Package
;; -*- emacs-lisp -*-
(unless package-archive-contents    ;; Refrech the packages descriptions
  (package-refresh-contents))
(setq package-load-list '(all))     ;; List of packages to load

(global-visual-line-mode t) ;; enable visual-line-mode globally
			    ;; so that every long line is "wrapped"
			    ;; at the right-side edge of the window/screen/buffer
			    
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-use-system-font t)
 '(inhibit-startup-screen t)    ; disable startup message
 '(package-selected-packages (quote (evil use-package)))
 '(scroll-bar-mode nil)         ; disable scroll-bar
 '(show-paren-mode t)           ; highlight matching brackets
 '(tool-bar-mode nil))          ; disable toolbar (in GUI)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DroidSans" :foundry "Bits" :slant normal :weight normal :height 98 :width normal))))
 '(menu ((t (:foreground "black" :weight semi-bold :height 1.0 :width normal :family "DroidSans"))))
 '(tty-menu-enabled-face ((t (:background "blue" :distant-foreground "black" :foreground "yellow" :weight bold :height 1.0 :width normal :family "DroidSans")))))

;;;; 20180625 aml -- emacs-25 + melpa:use-package --> melpa:evil
;;;; The evil package is initialized by use-package; the following enables it upon startup
(evil-mode 1)

;;;; THEME 
;;;; 20180625 aml -- emacs-25
;;;; the custom theme is bad or incomplete
; (when (display-graphic-p)
; (load-file "~/.emacs-color-theme")
    ; (my-color-theme))

;;;; use a built-in theme if/when in GUI
(when (display-graphic-p)
  (load-theme 'ample t))

(load-theme 'tango-dark t)
;;;; let's try enabling custom themes
(setq custom-safe-themes t)

 ;; the following lines were inserted to enable completion, style file
 ;; and/or multi-file in AUCTeX
;  (setq-default TeX-master nil)
; (setq TeX-parse-self t)
; (setq TeX-auto-save t)

;;; Enable php-mode autoloading and autorecognition of "*.php" files
; (autoload 'php-mode "php-mode.el" "Php mode." t)
; (setq auto-mode-alist (append '(("/*.\.php[345]?$" . php-mode)) auto-mode-alist))
