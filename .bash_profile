# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

# User specific environment and startup programs

PATH=$PATH:$HOME/.local/bin:$HOME/bin

JAVA_HOME="/usr/java/latest"
PLUGIN_HOME="/usr/java/latest/jre/plugin"
ANDROID_HOME="/usr/local/android-studio"

export PLUGIN_HOME
export JAVA_HOME
export ANDROID_HOME

export PATH=${JAVA_HOME}/jre/bin:${JAVA_HOME}/bin:${ANDROID_HOME}/bin:${ANDROID_HOME}/sdk/platform-tools:${ANDROID_HOME}/sdk/tools:${ANDROID_HOME}/sdk/build-tools:${PATH}

unset USERNAME


export PYTHONSTARTUP=$HOME/.pythonrc.py
