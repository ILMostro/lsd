# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=
# User specific aliases and functions
source $HOME/.aliases

export PS1='\[\033[96m\][$(date +%H\:%M)\[\033[49m\]]\[\033[35m\][\W]\[\033[1;34m\]\$\[\033[1;32m\] '
export LESS="--RAW-CONTROL-CHARS"
export EDITOR=vim

# Use colors for less, man, etc.
[[ -f ~/.LESS_TERMCAP ]] && . ~/.LESS_TERMCAP

#if (( $UID == 0 )); then
#  export PS1='\[\033[96m\][$(date +%H\:%M)\[\033[49m\]]\[\033[101m\][\!]\$\[\033[0m\] '
#else
#  export PS1='\[\033[96m\][$(date +%H\:%M)\[\033[49m\]]\[\033[35m\][\!]\$\[\033[39m\] '
#fi

### Default to installing pip packages to --user location
# export PIP_USER=yes

### chruby
# [[ -f /usr/local/share/chruby/chruby.sh ]] && . /usr/local/share/chruby/chruby.sh 
# [[ -f /usr/local/share/chruby/auto.sh ]] && . /usr/local/share/chruby/auto.sh 
