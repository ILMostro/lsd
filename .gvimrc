" Some things don't work as expected with GUI vim
" Mappings for easier copy/paste
nmap <S-Insert> "+gP
" imap <S-Insert> <Esc><S-Insert>i
imap <S-Insert> <Esc>"+gP
vmap <C-c> "+y

" For MacVim
" ==========
"
" Make window as big as possible; based on display of
" MacBook Pro
if has("gui_macvim")
  set lines=66
  set columns=203
  set guifont=Monaco:h13
endif
